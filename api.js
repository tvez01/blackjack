const { Router } = require("express");
const { MongoClient, ObjectId } = require("mongodb");

if (process.env.MODE == "production") {
  url = `mongodb://2217050:${process.env.MONGO_PASSWORD}@192.168.171.67`;
} else {
  url = "mongodb://localhost:27017";
}

const mongoClient = new MongoClient(url);
const db = mongoClient.db(
  process.env.MODE == "production" ? "2217050" : "blackjack"
);

const apiRouter = Router();

apiRouter.get("/user/:id", async (req, res) => {
  try {
    const users = db.collection("gamblers");
    const searchedUser = await users.findOne({
      _id: new ObjectId(req.params.id),
    });
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.post("/user/", async (req, res) => {
  try {
    const users = db.collection("gamblers");
    delete req.body.password;
    const newUser = await users.insertOne({
      ...req.body,
    });
    res.json(newUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.patch("/user/:id", async (req, res) => {
  try {
    const users = db.collection("gamblers");
    if (req.body.username) {
      const username = req.body.username;
      const updatedUser = await users.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: { username },
        }
      );
      res.json(updatedUser);
    } else {
      res.sendStatus(400);
    }
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.delete("/user/:id", async (req, res) => {
  try {
    const users = db.collection("gamblers");
    const deletedUSer = await users.deleteOne({
      _id: new ObjectId(req.params.id),
    });
    res.json(deletedUSer);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

module.exports = apiRouter;
